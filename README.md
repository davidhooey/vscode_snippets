# Visual Studio Code Snippets

Collection of user defined snippets for Visual Studio Code.

## Creating

Refer to the Visual Studio documentation.

https://code.visualstudio.com/docs/editor/userdefinedsnippets
https://vscode-docs.readthedocs.io/en/stable/customization/userdefinedsnippets/

## Installing

For Mac users, copy the `*.code-snippets` files to
`~/Library/Application Support/Code/User/snippets`

For Windows users, copy the `*.code-snippets` files to
`$HOME/Library/Application Support/Code/User/snippets/`

For Linux users, copy the `*.code-snippets` files to
`$HOME/.config/Code/User/snippets`
